import discord
import asyncio
from MoonScripts.CommandParser import CommandParser
from Framework.Commands.Jelly import Galaretka
from Framework.Commands.UpdateCommands import Activity_change as AC
from apscheduler.schedulers.asyncio import AsyncIOScheduler as scheduler

token = 'NjI1NzYyMDkwNTg1NDIzODc0.XYkWHQ.H8OCtwY-WOvoVIkNRTqYOChCAZY'
#token = 'NzU5NzAzNTk0MDA1NjkyNDE2.X3BXGw.jjSs1yaau9Di5WYEYxxJlhqCyGI'

commandParser = CommandParser()
discord.Permissions(administrator=True)
bot = discord.ext.commands.Bot(command_prefix = ';')
bot.fetch_offline_members = True
job = Galaretka()


@bot.event
async def on_ready():
    print('We have logged in as {0.user}'.format(bot))
    commandParser.BotLoad(bot)
    
    #schedule special jelly job
    job.user = bot.get_user(296355886711177216)
    sched = scheduler()
    sched.add_job(jelly_call, 'cron', hour='0', minute='0')
    sched.start()

#@bot.event
#async def on_message(message):
#    if message.author == bot.user:
#        return
    # if message.content.startswith('Gather'):                # Co to w ogóle jest, to jest zbiór wiadomości oryginalnej Moon?
    #     for channel in client.get_all_channels():
    #         if channel.type == discord.ChannelType.text:
    #             try:
    #                 async for message in channel.history(limit=None, before=None, after=None, around=None, oldest_first=True):
    #                     if message.author.id == 346926998448373760:
    #                         print(message.content)
    #             except discord.Forbidden:
    #                 print("no perms")
#    else:
#       await commandParser.ParseCommads(message, message.author)

@bot.event
async def on_member_update(before, after):
    comm = AC()
    await comm.Moral(before, after, bot)

# callable function to jelly job, just convience to omit call in form 'module:object_to_call'
async def jelly_call():
    await job.Post_jelly()


bot.run(token)