import discord
import asyncio
import timeit
import re
from discord.ext import commands

class TestCog(commands.Cog):

    def __init__(self, bot):
        self.client = bot

    @commands.command(name='tescior', pass_context=True)
    async def test(self, ctx):

        setup = '''import discord
import re'''
        timer = timeit.Timer('''with open("scenariusze/first_try.srt", 'r') as f:
            for line in f.readlines():
                line = re.sub(r'</i>|<i>|^#|#\s$', '', line).rstrip()''', setup=setup)
        print(f'Wynik re.sub : {timer.timeit(1000)}')

        timer = timeit.Timer('''with open("scenariusze/first_try.srt", 'r') as f:
            for line in f.readlines():
                line = line.rstrip().replace('</i>', '').replace('<i>', '').strip('#')''', setup=setup)
        print(f'Wynik rstrip + replace : {timer.timeit(1000)}')

        timer = timeit.Timer('''with open("scenariusze/first_try.srt", 'r') as f:
            translation = string.maketrans()
            for line in f.readlines():
                line = line.rstrip().replace('</i>', '').replace('<i>', '').strip('#')''', setup=setup)
        print(f'Wynik rstrip + replace : {timer.timeit(1000)}')



def setup(client):
    client.add_cog(TestCog(client))