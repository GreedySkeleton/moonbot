import discord
import asyncio
from discord.ext import commands


class Miau(discord.ext.commands.Cog):

    def __init__(self, bot):
        self.client = bot

    @commands.command(name='miau')
    async def Miau(self, ctx):
        voice_state = ctx.message.author.voice.channel
        if voice_state != None:
            vc = await voice_state.connect()
            source = await discord.FFmpegOpusAudio.from_probe("./moonbot/Aki_miau.mp3", executable="ffmpeg")
            vc.play(source, after=lambda a: print(a)) #for Linux path use -> ./moonbot/Aki_miau.mp3
            while vc.is_playing():
                await asyncio.sleep(1)
            await vc.disconnect()
            return 1


def setup(bot):
    bot.add_cog(Miau(bot))