import discord
import asyncio
import re
from gtts import gTTS
from io import BytesIO
from discord.ext import commands


class Krystyna(discord.ext.commands.Cog):
    
    def __init__(self, bot):
        self.script = None
        self.language = 'en'
        self.volume = None
        self.speed = False
        self.voice = None
        self.ctx = None
        self.vc = None
        self.client = bot
        self.count = 0

    async def prepare(self):
        with open(self.script, 'r') as f:
            for line in f.readlines():
                try:
                    line = line.rstrip().replace('</i>', '').replace('<i>', '').strip('#')
                    if re.search(r'^[0-9]|^[0-9]{2}:[0-9]{2}:[0-9]{2}[,][0-9]{3}', line) or not line:
                        continue
                    else:
                        await self.read(line)
                except ValueError:
                    continue
    
    async def read(self, line):#, player):
        speech = gTTS(text=line, lang=self.language, slow=self.speed)
        speech.save(f'./moonbot/scenariusze/kwestia.mp3') #for linux path -> ./moonbot/scenariusze/kwestia.mp3
        source = await discord.FFmpegOpusAudio.from_probe("./moonbot/scenariusze/kwestia.mp3", executable="ffmpeg") #for linux path -> ./moonbot/scenariusze/kwestia.mp3
        #speech.write_to_fp(player)
        #source = await discord.FFmpegPCMAudio.from_probe(source=player, executable="ffmpeg")
        self.vc.play(source)
        while self.vc.is_playing():
            await asyncio.sleep(0.1)

    @commands.command(name='Czubówna', pass_context=True)
    async def activate(self, ctx, *args):
        self.ctx = ctx
        attch = ctx.message.attachments[0]
        if attch.size < 5242880:
            if attch.filename[-4:] == '.srt':
                self.script = f'./moonbot/scenariusze/{attch.filename}' #for linux path ./moonbot/scenariusze/
                try:
                    await attch.save(self.script)
                except discord.HTTPException:
                    print('Failed to save file, aborting...')
                    self.ctx.send('Ojojoj, coś nie poszło >_>')
                    return
                self.vc = await self.client.get_channel(632580073685516308).connect()
                await self.prepare()
            else:
                await self.ctx.send('Przed wysłaniem upewnij się, że plik jest w formacie srt.\n🎭 Oszczędź głos Krystyny 📣, ma tylko jeden 🎭')
        else:
            await self.ctx.send('Co ty sobie myślisz zboczuchu😡! Wpychać w Krystynę coś tak wielkiego⛰️\n🛑 *W S T Y D Ź  S I Ę* 🛑')
        while self.vc.is_playing():
            await asyncio.sleep(1)
        await self.vc.disconnect()

    @commands.command(pass_context=True)
    @commands.has_any_role('Konglomerat', 'Pedał')
    async def alter_krystyna(self, ctx, *args):
        self.volume = None
        self.speed = None
        self.lektor = None

def setup(bot):
    bot.add_cog(Krystyna(bot))