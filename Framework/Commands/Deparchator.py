import requests
import cv2
import discord
import random
import re
import numpy as np

from discord.ext import commands
from Framework.Command import Command
from datetime import date


class Deparchator(discord.ext.commands.Cog):

    def __init__(self, bot):
        self.client = bot

    @commands.command(name='deparchuj')
    async def Deparchator(self, ctx):
        # if not await super().Execute(message, client):
        #     return False
        await ctx.message.channel.send('Wyszukiwanie szpiegów Mossadu...')
        for member in ctx.bot.get_all_members():
            await ctx.message.channel.send("Deparchuje: " + member.name)
            random.seed((date.today().strftime("%d%m%y") + member.name))
            randomNum = random.uniform(0.0,1.0)
            if randomNum < 0.2:
                await ctx.message.channel.send('Wykryto szpiega Mossadu: {0}', member.name)
                with requests.get(member.avatar_url) as r:
                    print(member.avatar_url)
                    img_data = r.content
                with open('parch.png', 'wb') as handler:
                    handler.write(img_data)

                image = cv2.imread("parch.png")
                height = 164
                width = 164
                dim = (width,height)
                background = cv2.resize(image,dim).astype(float)

                foreground = cv2.imread("gwiazda2.png").astype(float)

                alpha = cv2.imread("gwiazda_alpha.png")
                alpha = alpha.astype(float)/255/2

                background = cv2.multiply(1-alpha, background)
                foreground = cv2.multiply(alpha, foreground)

                result = background.copy()

                result = cv2.add(background, foreground)
                #result = cv2.addWeighted(background, 0.5, foreground, 0.5, 0)
                cv2.imwrite('parched.png', result)

                with open('parched.png', 'rb') as fp:
                    await ctx.message.channel.send(file=discord.File(fp, 'Szpieg.png'))
                                
        await ctx.message.channel.send('Zakończono detekcję szpionów')

        return 1

def setup(bot):
    bot.add_cog(Deparchator(bot))