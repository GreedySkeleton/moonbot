import discord

class Command:
    RequiredMessage = ''
    ContainsText = ''   #co to w ogole jest

    async def Execute(self, message, client):
        if not message.content.startswith(self.RequiredMessage):
            print(self.RequiredMessage)
            return False
        elif self.ContainsText not in message.content:      
            print(self.ContainsText)
            return False
        return True      
